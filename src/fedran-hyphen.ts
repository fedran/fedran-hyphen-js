import { ContentArgs, ContentPipeline } from "@mfgames-writing/contracts";
import * as Promise from "bluebird";

const words = require("an-array-of-english-words");

export function loadFedranHyphenPlugin(args: any): FedranHyphenPlugin
{
    return new FedranHyphenPlugin();
}

export class FedranHyphenPlugin implements ContentPipeline
{
    private separator: string;

    constructor(separator: string = "&#173;")
    {
        this.separator = separator;
    }

    public break(text: string): string
    {
        // There isn't much to this other than a simple regex search on
        // the contents, hanging on the accented word. First, we need to
        // set it up.
        const syllables = [
            "wa", "ra", "ma", "pa", "ba", "ha", "na", "da",
            "ta", "za", "sa", "ga", "ka", "fa",
            "we", "re", "me", "pe", "be", "he", "ne", "de",
            "te", "ze", "se", "ge", "ke", "fe",
            "wi", "ri", "mi", "pi", "bi", "hi", "ni", "chi",
            "ji", "shi", "gi", "ki", "fi",
            "wo", "ro", "mo", "po", "bo", "ho", "no", "do",
            "to", "jo", "so", "go", "ko", "fo",
            "wu", "ru", "mu", "pu", "bu", "hu", "nu", "tsu",
            "zu", "su", "gu", "ku", "fu",
            "rya", "mya", "pya", "bya", "hya", "nya", "chya",
            "jya", "shya", "gya", "kya",
            "ryo", "myo", "pyo", "byo", "hyo", "nyo", "chyo",
            "jyo", "shyo", "gyo", "kyo",
            "ryu", "myu", "pyu", "byu", "hyu", "nyu", "chyu",
            "jyu", "shyu", "gyu", "kyu",
            "a", "e", "i", "o", "u"
        ];
        let accents: string[] = [];

        for (var s of syllables)
        {
            accents.push(s
                .replace('a', 'à')
                .replace('e', 'è')
                .replace('i', 'ì')
                .replace('o', 'ò')
                .replace('u', 'ù'));
            accents.push(s
                .replace('a', 'ā')
                .replace('e', 'ē')
                .replace('i', 'ī')
                .replace('o', 'ō')
                .replace('u', 'ū'));
            accents.push(s
                .replace('a', 'á')
                .replace('e', 'é')
                .replace('i', 'í')
                .replace('o', 'ó')
                .replace('u', 'ú'));
        }

        // n has special rules.
        syllables.push("n(?![aeiouàèìòùāēīōūáéíóú])");

        // Combine them together into a massive pattern.
        var sPattern = "(" + syllables.join("|") + ")";
        var aPattern = "(" + accents.join("|") + "|" + syllables.join("|") + ")";

        // We need to build up from six to three syllables.
        var regex6 = new RegExp(sPattern + sPattern + sPattern + sPattern + aPattern + sPattern, 'gi');
        var regex5 = new RegExp(sPattern + sPattern + sPattern + aPattern + sPattern, 'gi');
        var regex4 = new RegExp(sPattern + sPattern + aPattern + sPattern, 'gi');
        var regex3 = new RegExp(sPattern + aPattern + sPattern, 'gi');

        // Perform the replacements.
        text = text.replace(
            regex6,
            (matched, a, b, c, d, p, f) =>
                words.indexOf(a + b + c + d + p + f) >= 0
                    ? matched
                    : [a, b, c, d, p + f].join(this.separator));
        text = text.replace(
            regex5,
            (matched, a, b, c, p, f) =>
                words.indexOf(a + b + c + p + f) >= 0
                    ? matched
                    : [a, b, c, p + f].join(this.separator));
        text = text.replace(
            regex4,
            (matched, a, b, p, f) =>
                words.indexOf(a + b + p + f) >= 0
                    ? matched
                    : [a, b, p + f].join(this.separator));
        text = text.replace(
            regex3,
            (matched, a, p, f) =>
                words.indexOf(a + p + f) >= 0
                    ? matched
                    : [a, p + f].join(this.separator));

        // Return the results.
        return text;
    }

    public process(content: ContentArgs): Promise<ContentArgs>
    {
        return new Promise<ContentArgs>((resolve, reject) =>
        {
            content.text = this.break(content.text);
            resolve(content);
        });
    }
}

export default loadFedranHyphenPlugin;
