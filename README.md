Hyphenation plugin for Fedran
=============================

This is a pipeline module which applies hyphenation to source files that use Fedran-specific words, such as Miwāfu names.
