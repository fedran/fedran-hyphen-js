# 1.0.0 (2018-08-12)


### Bug Fixes

* adding package management ([51a498c](https://gitlab.com/fedran/fedran-writing-hyphen-js/commit/51a498c))
